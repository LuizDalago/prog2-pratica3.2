
public class Pratica32 {
    public static void main(String args[]){
        System.out.println(densidade(-1,67,3));
    }
    public static double densidade(double x, double media, 
            double desvio) {
        double d = (1/Math.sqrt(Math.PI*2 * desvio)) *Math.pow(Math.E,(-0.5f*Math.pow((x - media)/desvio,2))) ;
        return d;
    }
}
